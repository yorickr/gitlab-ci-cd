package com.alten.cicd;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AppTest {
    private App a;

    @BeforeEach
    public void before() {
        a = new App();
    }

    @AfterEach
    public void after() {
        a = null;
    }

    @Test
    public void shouldAnswerWithTrue() {
        assertEquals(2, a.doThing(1, 2));
    }

    @Test
    public void mooiman() {
        assertEquals(-3, a.doThing(-1, 2));
    }
}
