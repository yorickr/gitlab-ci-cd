package com.alten.cicd;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        App a = new App();
        System.out.println(a.doThing(1, 9));
    }

    int doThing(int x, int y) {
        if (x > 0) {
            return x * y;
        } else
        {
            return x - y;
        }
    }
}
